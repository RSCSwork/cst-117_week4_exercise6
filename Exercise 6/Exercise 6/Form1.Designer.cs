﻿namespace Exercise_6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.diceRollBtn = new System.Windows.Forms.Button();
            this.dice1Lbl = new System.Windows.Forms.Label();
            this.dice2Lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // diceRollBtn
            // 
            this.diceRollBtn.Location = new System.Drawing.Point(13, 31);
            this.diceRollBtn.Name = "diceRollBtn";
            this.diceRollBtn.Size = new System.Drawing.Size(173, 203);
            this.diceRollBtn.TabIndex = 0;
            this.diceRollBtn.Text = "Roll the Dice!";
            this.diceRollBtn.UseVisualStyleBackColor = true;
            this.diceRollBtn.Click += new System.EventHandler(this.DiceRollBtn_Click);
            // 
            // dice1Lbl
            // 
            this.dice1Lbl.AutoSize = true;
            this.dice1Lbl.Location = new System.Drawing.Point(209, 107);
            this.dice1Lbl.MinimumSize = new System.Drawing.Size(100, 50);
            this.dice1Lbl.Name = "dice1Lbl";
            this.dice1Lbl.Size = new System.Drawing.Size(100, 50);
            this.dice1Lbl.TabIndex = 1;
            this.dice1Lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dice2Lbl
            // 
            this.dice2Lbl.AutoSize = true;
            this.dice2Lbl.Location = new System.Drawing.Point(324, 107);
            this.dice2Lbl.MinimumSize = new System.Drawing.Size(100, 50);
            this.dice2Lbl.Name = "dice2Lbl";
            this.dice2Lbl.Size = new System.Drawing.Size(100, 50);
            this.dice2Lbl.TabIndex = 2;
            this.dice2Lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 281);
            this.Controls.Add(this.dice2Lbl);
            this.Controls.Add(this.dice1Lbl);
            this.Controls.Add(this.diceRollBtn);
            this.Name = "Form1";
            this.Text = "Snake Eyes Dice Roller";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button diceRollBtn;
        private System.Windows.Forms.Label dice1Lbl;
        private System.Windows.Forms.Label dice2Lbl;
    }
}

