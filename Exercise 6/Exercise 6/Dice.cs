﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_6
{
    //the instructions said to make a class called Dice... 
    //naming convention common
    class Dice
    {
        private int numberOfSides; // numberof sides needed as required by the instructions, said to be private
        public int numberOfRolls; //dice need this field to return the number of rolls it took to get to the snake eyes roll
        Random randomNumber = new Random(); //need a random number generate to simulate a dice roll coming randomly with chances 1-6

        //constructor for the dice to determine number of sides. minimum requirement to create an object dice.
        public Dice(int numberOfSides)
        {
            //this check is to find out if the number was between 4 and 20 per instructions
            //throws a message if the number of dice sides is less than 4 or greater than 20.
            if (4 <= numberOfSides && numberOfSides <= 20)
            {
                //sets the number of sides this dice has
                this.numberOfSides = numberOfSides;
            }
            else
            {
                MessageBox.Show("The number of sides on the Dice at creation is less than 4 or greater than 20");
            }

        }//ends constructor dice

        //the instructions say to name the method this
        public int rollDie()
        {
            numberOfRolls++; //have to update the number of rolls each time this method is used
            return randomNumber.Next(1, numberOfSides + 1); //need to return the number that is on the face of the dice
            //this return must be a plus one in order to have range from 1 inclusive to the +1 which is excluded. 
        }//ends roll dice


    }//ends class dice
}//ends namespace
