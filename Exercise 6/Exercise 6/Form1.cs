﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void DiceRollBtn_Click(object sender, EventArgs e)
        {
            Dice leftDice = new Dice(6);
            Dice rightDice = new Dice(6);
            bool reachedSnakeEyesRoll = false;
            int leftDie = 0;
            int rightDie = 0;
            Random randomNumber = new Random();

            //using a do while because you want to roll the dice at least one time

            do
            {
                //this for loop is to create a tiny break so that the label can be updated in between rolls.
                //the goal here is to make the label look like it is spinning through the dice numbers 1-6
                for (int i = 0; i < 10; i++)
                {
                    dice1Lbl.Text = Convert.ToString(randomNumber.Next(1, 7));
                    dice2Lbl.Text = Convert.ToString(randomNumber.Next(1, 7));
                    i++;
                }//end for loop

                //first roll the dice
                leftDie = leftDice.rollDie();
                rightDie = rightDice.rollDie();
                //update the label with the dice number
                dice1Lbl.Text = Convert.ToString(leftDie);
                dice2Lbl.Text = Convert.ToString(rightDie);


                //short circuiting, if one dice isn't 1, than why bother checking if they're the same.
                if ((leftDie == 1) && (leftDie == rightDie))
                { 
                    //must be true in order to exit while condition
                    reachedSnakeEyesRoll = true;
                }//ends if

            } while (reachedSnakeEyesRoll == false);

            //using a message box to try something different.
            MessageBox.Show(" It took, " + leftDice.numberOfRolls + " rolls to get Snake Eyes");

        }//ends dice roll btn
    }//ends class
}//ends namespace
